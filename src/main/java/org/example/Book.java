package org.example;

import java.util.ArrayList;
import java.util.List;

class Author {
    private final String name;
    private String surname = "";

    public Author(String name) {
        this.name = name;
    }

    public Author(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void print() {
        System.out.println("Author: " + name);
    }
}

interface Element {
    void print();

    void add(Element e);

    void remove(Element e);

    Element get(int index);
}

class Image implements Element {
    private final String url;

    public Image(String url) {
        this.url = url;
    }

    @Override
    public void print() {
        System.out.println("Image with name: " + url + "\n");
    }

    @Override
    public void add(Element e) {

    }

    @Override
    public void remove(Element e) {

    }

    @Override
    public Element get(int index) {
        return null;
    }
}

class Paragraph implements Element {
    private final String text;

    public Paragraph(String paragraph) {
        this.text = paragraph;
    }

    @Override
    public void print() {
        System.out.println("Paragraph: " + text + "\n");
    }

    @Override
    public void add(Element e) {

    }

    @Override
    public void remove(Element e) {

    }

    @Override
    public Element get(int index) {
        return null;
    }
}

class Table implements Element {
    private final String something;

    public Table(String something) {
        this.something = something;
    }

    @Override
    public void print() {
        System.out.println("Table with title: " + something + "\n");
    }

    @Override
    public void add(Element e) {

    }

    @Override
    public void remove(Element e) {

    }

    @Override
    public Element get(int index) {
        return null;
    }
}

class TableOfContents implements Element {
    private final String something;

    public TableOfContents(String something) {
        this.something = something;
    }

    @Override
    public void print() {
        System.out.println("TableOfContents: " + something + "\n");
    }

    @Override
    public void add(Element e) {

    }

    @Override
    public void remove(Element e) {

    }

    @Override
    public Element get(int index) {
        return null;
    }
}

class Section implements Element {
    protected final String title;
    private final List<Element> elements;

    public Section(String title) {
        this.title = title;
        elements = new ArrayList<>();
    }

    @Override
    public void print() {
        System.out.println(title);
        for (Element e : elements) {
            e.print();
        }
    }

    @Override
    public void add(Element e) {
        elements.add(e);
    }

    @Override
    public void remove(Element e) {
        elements.remove(e);
    }

    @Override
    public Element get(int index) {
        return elements.get(index);
    }
}

public class Book extends Section {
    private final List<Author> authors;

    public Book(String title) {
        super(title);
        authors = new ArrayList<>();
    }

    void addAuthor(Author author) {
        authors.add(author);
    }

    void addContent(Element e) {
        super.add(e);
    }

    @Override
    public void print() {
        System.out.println("Book: " + super.title);
        System.out.println("Authors\n");
        for (Author a : authors) {
            a.print();
        }
        super.print();
    }

}
